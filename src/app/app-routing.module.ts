import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AccueilComponent} from './shared/cadre/accueil/accueil.component';
import {InviteComponent} from './personne/invite/invite/invite.component';
import {EtudiantComponent} from './personne/etudiant/etudiant/etudiant.component';
import {EnseignantComponent} from './personne/enseignant/enseignant/enseignant.component';
import {AdminComponent} from './personne/admin/admin.component';
import {EmployeComponent} from './personne/employe/employe.component';
import {EtudiantListComponent} from './personne/etudiant/etudiant-list/etudiant-list.component';
import {EtudiantCreerComponent} from './personne/etudiant/etudiant-creer/etudiant-creer.component';
import {EtudiantDetailComponent} from './personne/etudiant/etudiant-detail/etudiant-detail.component';
import {EtudiantParIdComponent} from './personne/etudiant/etudiant-par-id/etudiant-par-id.component';


const routes: Routes = [

  {path: '', redirectTo: '/accueil', pathMatch: 'full'},
  {path: 'accueil', component: AccueilComponent},
  {path: 'invite', component: InviteComponent},
  {
    path: 'etudiant',
    component: EtudiantComponent,
    children: [
      {
        path: '',
        component: EtudiantListComponent

      },
      {
        path: 'creer',
        component: EtudiantCreerComponent

      },
      {
        path: 'id/detail',
        component: EtudiantDetailComponent

      },
      {
        path: 'id',
        component: EtudiantParIdComponent

      },
    ]
  },
  {
    path: 'enseignant',
    component: EnseignantComponent
  },
  {
    path: 'administration',
    component: AdminComponent
  },
  {
    path: 'employe',
    component: EmployeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
