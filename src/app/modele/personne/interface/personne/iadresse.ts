export interface Iadresse {
  codepostal?: string;
  quartier?: string;
  ville?: string;
  email?: string;
}
