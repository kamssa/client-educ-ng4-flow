import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {Resultat} from '../../../../modele/resultat';
import {Invite} from '../../../../modele/personne/invite';


import {Iadresse} from '../../../../modele/personne/interface/personne/iadresse';
import {Iinvite} from '../../../../modele/personne/interface/personne/iinvite';
import {Observable} from 'rxjs/Observable';
@Injectable()
export class InviteService {
  private urlInvite = 'http://localhost:8080/typepersonnes/IN';
  private urlPersonne = 'http://localhost:8080/personnes';

  constructor(private http: Http) {

  }

  getAllInvite(): Observable<Resultat<Iinvite[]>> {
    return this.http.get(this.urlInvite)
      .map(res => res.json())
      .do(data => console.log(data));
  }

  ajoutInvite(invite: Invite): Observable<Resultat<Invite>> {
    const add: Iadresse = {
      codepostal: invite.adresse.codepostal,
      quartier: invite.adresse.quartier,
      ville: invite.adresse.ville,
      email: invite.adresse.email,
    };
    const inv: Iinvite = {
      titre: invite.titre,
      nom: invite.nom,
      prenom: invite.prenom,
      cni: invite.cni,
      login: invite.login,
      password: invite.password,
      actived: invite.actived,
      description: invite.description,
      adresse: add,
      type: 'IN',
      raison: invite.raison,
      profil: invite.profil,
      societe: invite.societe,
      statut: invite.statut
    };
    return this.http.post('http://localhost:8080/personnes', inv)
      .map(res => res.json())
      .do(data => console.log(data));

  }

  modifierUnInvite(modifInvite: Invite): Observable<Resultat<Invite>> {
    let add: Iadresse;
    add = {
      codepostal: modifInvite.adresse.codepostal,
      quartier: modifInvite.adresse.quartier,
      ville: modifInvite.adresse.ville,
      email: modifInvite.adresse.email
    };
    let inviteModif: Iinvite
    inviteModif = {
      id: modifInvite.id,
      version: modifInvite.version,
      titre: modifInvite.titre,
      nom: modifInvite.nom,
      prenom: modifInvite.prenom,
      cni: modifInvite.cni,
      login: modifInvite.login,
      password: modifInvite.password,
      actived: modifInvite.actived,
      description: modifInvite.description,
      adresse: add,
      type: 'IN',
      raison: modifInvite.raison,
      profil: modifInvite.profil,
      societe: modifInvite.societe,
      statut: modifInvite.statut
    };
    return this.http.put(this.urlPersonne, inviteModif)
      .map(res => res.json())
      .do(data => {
        console.log('apres service');
        console.log(data);
      })
      .catch(this._errorHandler);

  }

  // supprimer un invite
  supprimerUnInvite(id: number): Observable<Resultat<boolean>> {
    return this.http.delete(`${this.urlPersonne}/${id}`)
      .map(res => res.json())
      .do(data => console.log(data))
      .catch(this._errorHandler);
  }

  ///////////////////////////////////////////
  // recuper les errurs
  _errorHandler(err) {
    let erreMessage: string;
    if (err instanceof Response) {
      const body = err.json() || '';
      const erreur = body.error || JSON.stringify(body);
      erreMessage = `${err.status} - ${err.statusText} || "}" ${erreur}`;

    } else {
      erreMessage = err.message ? err.message : err.toString();
    }

    return Observable.throw(erreMessage);
  }

}
