import {Component, OnInit} from '@angular/core';
import {Iinvite} from '../../../modele/personne/interface/personne/iinvite';
import {Invite} from '../../../modele/personne/invite';

import {Adresse} from '../../../modele/adresse/adresse';
import {InviteService} from '../../../shared/service/personne/invite/invite.service';


@Component({
  selector: 'app-invite',
  templateUrl: './invite.component.html',
  styleUrls: ['./invite.component.scss']
})
export class InviteComponent implements OnInit {
  invites: Iinvite[];
  _status: number;
  invite: Invite;
  selectedInvite: Invite;
  displayDialog: boolean;
  dialogVisible: boolean;
  newInvite: boolean;
  msg: string[];
  errorMessage: string;
  errorMessageStatus: string;

  constructor(private  inviteService: InviteService) {
  }

  ngOnInit() {
    this.tousInvites();
  }

  tousInvites() {
    this.inviteService.getAllInvite()
      .subscribe(data => {
          this.invites = data.body;
          this._status = data.status;
          this.msg = data.messages;
        },
        error => {
          if (error.status = 0) {
            this.errorMessageStatus = 'Problème de conxion!';
          } else {
            this.errorMessage = error;
          }
        });
  }


  showDialogToAdd() {
    this.newInvite = true;

    const ad = new Adresse(null, null, null, null);
    this.invite = new Invite(null, null, null, null, null, null, null, null, null, null, null, null, false, null, null, ad, null);
    this.displayDialog = true;
  }

  save() {
    if (this.newInvite) {
      console.log('debut ajout');
      console.log(this.invite);
      this.inviteService.ajoutInvite(this.invite)
        .subscribe(res => {
            this.invites.push(res.body);
            // console.log(res);
          },
          error => this.errorMessage = error);

    } else {
      console.log('modif');
      console.log(this.invite);
      this.inviteService.modifierUnInvite(this.invite)
        .subscribe(res => {
            this.invites[this.findSelectedInviteIndex()] = res.body;
            console.log(res.body);
          },
          resError => {
            this.errorMessage = resError;
            console.error(resError);
          }
        );
    }
    this.invite = null;
    this.displayDialog = false;

  }

  delete() {
    this.inviteService.supprimerUnInvite(this.invite.id)
      .subscribe(res => {
        this.invites.splice(this.findSelectedInviteIndex(), 1);
        console.log(res);
      }, erreur => this.errorMessage = erreur);
    this.displayDialog = false;
  }

  onRowSelect(event) {
    this.newInvite = false;
    this.invite = this.cloneInvite(event.data);
    this.displayDialog = true;
    console.log(event.data);
    console.log(this.invite);
  }

  findSelectedInviteIndex(): number {
    return this.invites.indexOf(this.selectedInvite);
  }

  cloneInvite(i: Iinvite): Invite {
    let invite: Invite;
    invite = new Invite();
    for (const prop in i) {
      invite[prop] = i[prop];
    }
    return invite;
  }

}
