import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtudiantCreerComponent } from './etudiant-creer.component';

describe('EtudiantCreerComponent', () => {
  let component: EtudiantCreerComponent;
  let fixture: ComponentFixture<EtudiantCreerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtudiantCreerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtudiantCreerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
