import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EtudiantParIdComponent } from './etudiant-par-id.component';

describe('EtudiantParIdComponent', () => {
  let component: EtudiantParIdComponent;
  let fixture: ComponentFixture<EtudiantParIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EtudiantParIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EtudiantParIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
