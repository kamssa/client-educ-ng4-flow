import { ClientEducNg4FlowPage } from './app.po';

describe('client-educ-ng4-flow App', () => {
  let page: ClientEducNg4FlowPage;

  beforeEach(() => {
    page = new ClientEducNg4FlowPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
